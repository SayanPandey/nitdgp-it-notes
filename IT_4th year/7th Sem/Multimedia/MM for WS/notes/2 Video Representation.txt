Video Representation

-Video Signal Representation
	-The human eye views pictures and motion pictures.
	-In conventional TV sets, video signal representation is with use of CRT (cathode ray tube).
	An electron beam carries corresponding pattern information, such as intensity in viewed scene.
	Analysis of video signal is on basis of signal coming from a camera.

	-The televised image should convey the spatial & temporal content of the scene. Important measures are:
		-Vertical details & viewing distance
			-The geometry of the field occupied by the TV image is based on the ratio of the picture width to height W/H which is called the aspect ratio; conventionally it is 4/3. 
			-The viewing distance D determines the angle h subtended by the picture height. The measure of angle is given by D/h.
			-Smallest detail that can be reproduced in the image is pixel. Ideally, each detail of a scene needs a pixel. However, only 70% vertical detail is presented by the scanning lines as some details is lost in between the lines. This ratio is called the Kell factor.
		-Horizontal detail & picture width
			-Picture width = 4/3 * Picture height.
		-Total detail content of the image:
			-Vertical resolution = no. of pixels in picture * height 
			-Picture width in pixels = Aspect ratio * Hor. Resolution
		-Perception of depth
			-Third spatial dimension (depth) depends upon angular separation of images received by two eyes of viewer.
			-In flat image TV, degree of depth is perceived from perspective appearance of subject matter.
			-Also, focal length of lenses influences depth perception.
		-Luminance & chrominance
			-Color vision is achieved through 3 signals – R,G,B that light each portion of scene.
			-These 3 signals are conveyed separately to the input terminals of the picture tube so that the tube reproduces at each point the relative intensities of R, G, B discerned by the camera.
			-During transmission of the signals from camera to receiver (display), a different division of signals in comparison to RGB is used.
			-The color encoding during transmission uses luminance & two chrominance signals.
		-Temporal aspects of illumination: 
			-A discrete sequence of individual pictures can be perceived as a continuous sequence if the slightly different individual pictures or frames can be rapidly shown in succession & light is cut off between the frames.
			-For perception of continuity, two things are needed:
				-Rate of image repetition must be high for smooth motion from frame to frame.
				-Rate must be high enough so that persistence of vision extends between flashes. This property of human vision is called boundary of motion resolution.
		-Continuity of motion
			-Perception of continuous motion is with frame rate>15 frames /sec.
			-Smooth video motion is at 30 frames/sec.
			-For motion picture it is 24 frames/sec but appears smooth as it is shown in big screen at dark, away from the viewer.
		-Flicker
			-Through slow motion, a periodic fluctuation of brightness perception occurs called flicker.
			-To avoid it, min frame rate should be > 50 refresh cycles/sec.
			-A movie at 16 frames/sec has flicker for which light wave is interrupted twice additionally to reduce flicker. So, picture flicker rate = 3X16 = 48 Hz.
			-In TV
				-Flicker is reduced through use of display refresh buffer.
				-A full TV picture is divided to two half pictures consisting of interleaved scanning lines.
				-Each half picture is transmitted in an interleaved manner.
				-If picture transmission occurs at 25 Hz, the half pictures must be scanned at 2X25 = 50 Hz.
				-This technique is called Interlacing where each image is divided to two frames having alternate line.
				-The even horizontal line are scanned once & then the odd horizontal lines.
				-This reduces flicker without increasing bandwidth.
		-Blanking
			-Horizontal blanking
				-As the beam scans from left to right, the electron beam of the picture tube of TV is turned on & intensity is modulated.
				When it flies back from right to left side of next line, electron beam is blanked/turned off.
				-This is called horizontal blanking & the period for which it is blanked off is called horizontal blanking period.
			-Vertical blanking
				-When beam reaches the right hand side of the bottom most horizontal line of an image to be scanned of a frame, it moves back quickly to the left topmost corner of ext frame.
				This diagonal movement is blanked off & called Vertical blanking.
		-Still image coding
			-Line drawings, photographs, text page etc. can be coded using
				-Vector graphics
					-Every image is represented as a collection of line vectors.
					-It is no more in dominant use except for drafting.
				-Raster Scan
					-Image is scanned from the top left cornet to bottom right corner in sequential order.
					-Image is divided to a collection of horizontal lines.
		-Color Models
			-Color perception
				-We see an object when light falling on it within visible range of spectrum and is reflected & detected by the eye.
				-Each color radiation has three properties: luminance, hue & saturation – the corresponding human perception being brightness, color & purity.
					-Luminance: Property by virtue of which a radiation appears to have more or less intensity. 
					-Hue: Different hues of same color are produced by radiation of different wavelength.
					-Saturation: The colorfulness of a color. When a color has more white mixed with it, it is less saturated.
				-The human eye’s response is not same for all colors. It responds maximum to yellow. It also responds more to variations in brightness than to color. These two factors are used in image compression by discarding color components not visually significant.
			-Chromatic Models
				-One parameter defines luminance or brightness, the other two defines color of pixels called chrominance.
				-Two chromatic components are required which are added to get the final color.
				-RGB Model:
					-Additive model
					-Originally for image capture & display
					-R+G+B = 1
				-CMYK Model
					-Subtractive model
					-Cyan, magenta, yellow & black represent the image
				-YUV Model
					Y stands for luminance , U,V for two color components.

					Y = 0.299R + 0.587G + 0.114B
					U = 0.596R - 0.247G - 0.322B
					V = 0.211R – 0.532G + 0.312B

					-There are several YUV models another of which is:
						Y = 0.3R + 0.6G + 0.1B
						U = B-Y
						V = R-Y
					If R=G=B=0; Y=U=V=0; it is a monochrome grayscale image.
	