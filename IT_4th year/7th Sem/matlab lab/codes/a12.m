clc;
fun = [1,2,-1];
coeff = [2,1,1;4,2,3;2,5,5];
const = [14;28;30];
eqs = [1;1;1];
sign = [1,1,1];
slack = eye(3);
table = [zeros(1,1),fun,zeros(1,4);zeros(3,1),coeff,slack,const;zeros(1,8)];
for i=2:4
    tmp = -table(1,i);
    for j=2:4
        tmp = tmp + (table(j, i)*table(j, 1));
    end
    table(5,i)=tmp;
end
minim = min(table(5,:));
while minim<0
    index = find(table(5,2:7)==min(table(5,2:7)));
    index = index+1;
    mini = inf;
    in = 0;
    for i=2:4
        tmp = table(i, 8)/table(i, index);
        if tmp<mini && tmp>0
            mini = tmp;
            in = i;
        end
    end
    pivot = table(in, index);
    for j=2:8
        table(in, j) = table(in, j)/pivot;
    end
    for i=2:5
        if i~=in
            tmp=table(i, index);
            for j=2:8
                table(i,j) = table(i,j)-((tmp*table(in,j)));
            end
        end
    end
    table(in, 1) = table(1,index);
    minim = min(table(5,:));
end
disp(table);