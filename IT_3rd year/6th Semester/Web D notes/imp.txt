HTTP: Hypertext Transfer Protocol
RFC: Request for Comments
TCP: Transmission Control Protocol
URI: Uniform Resource Identifier
WINS: Windows Internet Naming Service
DNS: Domain Name System
FQDN: Fully Qualified Domain Names
PQDN: Partially Qualified Domain Names
ICMP: Internet Control Message Protocol
UA: User Agent
MTA: Message Transfer Agent
MAA: Message Access Agent
MIME: Multipurpose Internet Mail Extensions
NVT: Network Virtual Terminal
SMTP: Simple Mail Transfer Protocol
POP3: Post Office Protocol, version 3
IMAP4: Internet Mail Access Protocol, version 4
RSA: Rivest Shamir Adleman